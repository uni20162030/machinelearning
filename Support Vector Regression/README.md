This is a Support Vector Regression model predicting the length of bluegills fish from its age. It uses the Bluegills.csv as the training data and it uses Feature scaling before training the model.
